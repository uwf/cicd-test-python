import src.demo.example as example

def test_add_one() -> None:
    """Validates that the add_one function can do 1+1"""
    expected = 2
    result = example.add_one(1)
    assert result == expected
