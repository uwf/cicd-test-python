def add_one(number: int) -> int:
    """Add one"""
    return number + 1